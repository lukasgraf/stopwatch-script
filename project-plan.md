# Stopwatch Script
by Lukas Graf

Began 2020-07-15
Ended 2020-07-15

## Abstract
This is a simple script to measure the time spent working on a task.

## Context
I have had problems getting accurate measures of time spent on any particular task at my computer. Glancing on the clock and remembering the start-, stop- and break-times appears to be too effortous and not habitual enough. Opening some folders and programs when beginning work, and closing the laptop or setting the tower computer to stand-by, are already habits, so I want my time-tracking solution to work around those.

## Constraints
### Schedule
There is no mandatory deadlines. However, I prefer concluding this project in a day or two.

### Finances
There are no expected costs involved.

### Features
Mandatory, in order of priority:
* count timespan between starting and stopping
* recognise when the computer goes to standby and remove that time from the total timespan
* allow user to reset and show timespan until that point of time

also desired:
* configuration options where reasonable

## Technologies
* Python
* Threads

## Progression
* 2020-07-15
 * 45 Minutes: first draft
 * 36 Minutes: debugging and tidying up
 * 11 Minutes: thorough testing
 *  8 Minutes: preparing configuration and instructions
 *  9 Minutes: Python 2.x compatibility
 *  2 Minutes: better quitting, shorter interval

## Conclusions
None

## References
* [Python online documentation](https://docs.python.org/)
* [EOF error when giving input()](https://www.reddit.com/r/learnpython/comments/1ujc9w/python_33how_do_you_use_input_im_getting_eof_error/)
* [variable scope patching](https://python-forum.io/Thread-How-to-fix-UnboundLocalError)

## Additional Notes
None

