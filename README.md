# Stopwatch Script

A simple script to help me track how long I spend working on the computer.

## Usage

* Run with Python (does not matter if 2.x or 3.x)
* Press Enter at any time to get the time since starting the script
* Press Enter again to get the time since last pressing Enter
* Type "exit" and press Enter to end the process
