import threading, time, math, sys

#
# SHARED VARIABLES
#

# Configure: shorter interval means more precision regarding breaks/interruptions
tick_interval = 60 # in seconds
# Configure: tolerance should be less than interval, but greater than 0
# interval - tolerance = how long a break has to be before it can get detected
tick_tolerance = 10 # in seconds

start_time = None
lasttick_time = None
break_sum = 0

#
# TICK THREAD
#

quit_event = threading.Event()

def tick_def():
    global lasttick_time, break_sum
    # loop until main thread says to quit
    while not quit_event.wait( tick_interval ):
        # check if there was an interruption
        if lasttick_time and lasttick_time + tick_interval + tick_tolerance < time.time():
            # add duration of interruption to the sum
            break_sum = time.time() - lasttick_time
        # prepare for next cycle in the loop
        lasttick_time = time.time()

tick_thread = threading.Thread( None, tick_def )
tick_thread.start()

#
# MAIN THREAD
#

should_run = True
while should_run:
    # prepare for this phase
    start_time = time.time()
    lasttick_time = start_time
    break_sum = 0

    # wait until phase is over
    if sys.version_info[0] < 3:
        should_run = raw_input("Press Enter to get the passed time, or type 'exit' to end the script.") != "exit"
    else:
        should_run = input("Press Enter to get the passed time, or type 'exit' to end the script.") != "exit"

    # give information about phase
    print("The phase between {} and {} lasted {:.1f} minutes.\n".format( time.strftime("%H:%M", time.localtime(start_time)), time.strftime("%H:%M"), (time.time() - start_time - break_sum) / 60 ))

# quit procedure, just in case KeyboardInterrupt does not work as expected
quit_event.set()
tick_thread.join()
